<?php
/**
 * @author Denysaw
 */
namespace common\jobs;

use common\models\Toss;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class GiftShippingJob extends BaseObject implements JobInterface
{

    /** @var int */
    public $toss_id;


    public function execute($queue)
    {
        $toss    = Toss::findOne(['id' => $this->toss_id]);
        $giftId  = $toss->data;
        $address = $toss->user->address;

        // TODO: implement informing couriers by mail/messengers

        $toss->status = Toss::STATUS_SHIPPED;
        $toss->save();
    }
}