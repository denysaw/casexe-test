<?php
/**
 * @author Denysaw
 */
namespace common\jobs;

use common\models\Toss;
use common\models\Prize;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class ProcessPrizeJob extends BaseObject implements JobInterface
{

    /** @var int */
    public $toss_id;


    public function execute($queue)
    {
        $toss = Toss::findOne(['id' => $this->toss_id]);

        switch ($toss->prize->type) {
            case Prize::TYPE_MONEY:
                $queue->push(new MoneyTransferJob(['account' => $toss->user->account, 'amount' => $toss->data, 'toss_id' => $toss]));
                break;

            case Prize::TYPE_TOPUP:
                $queue->push(new BalanceTopUpJob(['toss_id' => $toss]));
                break;

            case Prize::TYPE_GIFT:
                $queue->push(new GiftShippingJob(['toss_id' => $toss]));
        }
    }
}