<?php
/**
 * @author Denysaw
 */
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'points_rate' => 5,
    'money' => [
        'min'  => 100,
        'max'  => 10000,
        'step' => 100
    ],
    'points' => [
        'min'  => 10,
        'max'  => 1000,
        'step' => 10
    ],
    'prizeTypes' => [
        'money', 'topup', 'gift'
    ],
    'toss.statuses' => [
        'created', 'refused', 'pending_delivery', 'converted', 'shipped', 'complete', 'failed'
    ]
];
