<?php
/**
 * @maintainer Denysaw
 */
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'queue' => [
	        'class' => \yii\queue\file\Queue::class,
	        'path' => '@console/runtime/queue',
	        'as log' => \yii\queue\LogBehavior::class,
        ],
        'game' => [
            'class' => 'common\components\GameComponent',
        ],
        'converter' => [
            'class' => 'common\components\ConverterComponent',
        ],
    ],
    'bootstrap' => [
	    'queue',
    ],
];
