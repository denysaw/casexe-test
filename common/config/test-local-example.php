<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=default',
            'username' => 'default',
            'password' => 'secret',
            'charset' => 'utf8',
        ],
    ],
];
