<?php
/**
 * @author Denysaw
 */
namespace common\models;

/**
 * This is the model class for table "prize".
 *
 * @property int $id
 * @property string $type
 * @property int $is_available
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Toss[] $tosses
 */
class Prize extends ModelBase
{

    const TYPE_MONEY = 'money';
    const TYPE_TOPUP = 'topup';
    const TYPE_GIFT  = 'gift';

    const TYPES = [
        self::TYPE_MONEY => 'Деньги',
        self::TYPE_TOPUP => 'Пополнение баланса',
        self::TYPE_GIFT  => 'Подарок',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prize';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['is_available', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'string', 'max' => 255],
            [['type'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'is_available' => 'Is Available',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTosses()
    {
        return $this->hasMany(Toss::class, ['prize_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPES[$this->type];
    }

    /**
     * @param int $data
     * @return string
     */
    public function getDetails(int $data): string
    {
        if (!$data) {
            return 'К сожалению, ' . ($this->type == self::TYPE_MONEY ? 'деньги' : 'подарки') . ' закончлись ;(';
        }

        $text = 'Ошибка, обратитесь за призом к администратору сайта.';

        switch ($this->type) {
            case self::TYPE_MONEY:
                $text = 'На сумму: ' . $data . 'Kč';
                break;

            case self::TYPE_TOPUP:
                $text = 'На ' . $data . ' баллов';
                break;

            case self::TYPE_GIFT:
                $text = 'Игрушка "' . Gift::findOne(['id' => $data])->name . '"';
        }

        return $text;
    }
}