<?php
/**
 * @author Denysaw
 */
namespace common\models;

/**
 * This is the model class for table "shipment".
 *
 * @property int $id
 * @property int $user_id
 * @property int $toss_id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Toss $toss
 * @property User $user
 */
class Shipment extends ModelBase
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'toss_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['toss_id'], 'exist', 'skipOnError' => true, 'targetClass' => Toss::class, 'targetAttribute' => ['toss_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'toss_id' => 'Toss ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToss()
    {
        return $this->hasOne(Toss::class, ['id' => 'toss_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
