<?php
/**
 * @author Denysaw
 */
namespace common\models;

/**
 * This is the model class for table "budget".
 *
 * @property int $id
 * @property string $name
 * @property int $amount
 * @property int $created_at
 * @property int $updated_at
 */
class Budget extends ModelBase
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'budget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['amount', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return Budget
     */
    public static function main(): Budget
    {
        return self::findOne(['name' => 'main']);
    }
}
