<?php
/**
 * @author Denysaw
 */
namespace common\models;

/**
 * This is the model class for table "payout".
 *
 * @property int $id
 * @property int $user_id
 * @property int $toss_id
 * @property int $amount
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Toss $toss
 * @property User $user
 */
class Payout extends ModelBase
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payout';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required'],
            [['user_id', 'toss_id', 'amount', 'created_at', 'updated_at'], 'integer'],
            [['toss_id'], 'exist', 'skipOnError' => true, 'targetClass' => Toss::class, 'targetAttribute' => ['toss_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'toss_id' => 'Toss ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToss()
    {
        return $this->hasOne(Toss::class, ['id' => 'toss_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
