<?php
/**
 * @author Denysaw
 */
namespace common\tests\unit\models;

use Yii;
use common\models\User;
use common\models\Toss;
use common\models\Prize;

/**
 * Converter test
 */
class ConverterTest extends \Codeception\Test\Unit
{

    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testMoneyToPoints()
    {
        $amount = 500;
        $rate = Yii::$app->params['points_rate'];

        $toss = new Toss();
        $toss->user_id  = User::find()->orderBy('rand()')->one();
        $toss->prize_id = Prize::findOne(['type' => Prize::TYPE_MONEY])->id;
        $toss->data = $amount;
        $toss->save();

        Yii::$app->converter->convert($toss);

        expect('toss should be with prize of balance topUp type', $toss->prize->type)->equals(Prize::TYPE_TOPUP);
        expect('toss amount data should be divided with points rate', $toss->data)->equals($amount / $rate);
    }


    public function testPointsToMoney()
    {
        $amount = 500;
        $rate = 5;

        $toss = new Toss();
        $toss->user_id  = User::find()->orderBy('rand()')->one();
        $toss->prize_id = Prize::findOne(['type' => Prize::TYPE_MONEY])->id;
        $toss->data = $amount;
        $toss->save();

        Yii::$app->converter->convert($toss);

        expect('toss should be with money prize type', $toss->prize->type)->equals(Prize::TYPE_MONEY);
        expect('toss amount data should be multiplied with points rate', $toss->data)->equals($amount * $rate);
    }
}
