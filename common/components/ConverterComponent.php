<?php
/**
 * @author Denysaw
 */
namespace common\components;

use Yii;
use yii\base\Component;
use common\models\Toss;
use common\models\Prize;

/**
 * Class ConverterComponent
 * @package common\components
 */
class ConverterComponent extends Component
{

    /**
     * @param Toss $toss
     * @return bool
     */
    public function convert(Toss $toss): bool
    {
        $rate = Yii::$app->params['points_rate'];
        $type = $toss->prize->type;

        $map = [
            Prize::TYPE_MONEY => Prize::TYPE_TOPUP,
            Prize::TYPE_TOPUP => Prize::TYPE_MONEY,
        ];

        if (!in_array($type, array_keys($map))) {
            // TODO: log error
            // Yii::getLogger()->log('error');
            return false;
        }

        $toss->prize_id = Prize::findOne(['type' => $map[$type]]);
        $type == Prize::TYPE_MONEY ? $toss->data /= $rate : $toss->data *= $rate;
        $toss->save();

        return true;
    }
}