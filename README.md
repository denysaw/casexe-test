#Casexe Test App
Test Yii2 application for Casexe s.r.o.

## Installation
```bash
git clone git@gitlab.com:denysaw/casexe.git
cd casexe
./up
```
it will install composer packages, start docker containers, init Yii, migrate and seed DB.

## Run
### Browser:
Navigate to `http://localhost/`<br>
Username: `test`<br>
Password: `test`<br>

### Console:
To perform MoneyTransfer you should:
```bash
./yii money-transfer <int batch size, default: 100>
```

### Tests:
To run ConverterTest you should:
```bash
./vendor/bin/codecept run -c common
```

##Author's comments:
Конечно, тут можно улучшать, геттеры/сеттеры юзать вместо открытых паблик-полей, логировать и т.д. но не было времени )<br>
Приятного Code-Review и тестирования ;)