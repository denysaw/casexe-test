<?php
/**
 * @author Denysaw
 */
use yii\db\Migration;

/**
 * Handles the creation of table `{{%gift}}`.
 */
class m190419_185440_create_gift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%gift}}', [
            'id'         => $this->primaryKey(),
	        'name'       => $this->string()->notNull()->unique(),
	        'price'      => $this->integer()->unsigned()->notNull(),
	        'quantity'   => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gift}}');
    }
}
