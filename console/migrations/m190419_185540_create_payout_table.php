<?php
/**
 * @author Denysaw
 */
use yii\db\Migration;

/**
 * Handles the creation of table `{{%payout}}`.
 */
class m190419_185540_create_payout_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payout}}', [
            'id' => $this->primaryKey(),
	        'user_id'    => $this->integer()->notNull(),
	        'toss_id'    => $this->integer(),
	        'amount'     => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

	    $this->addForeignKey('fk-payout-user_id', 'payout', 'user_id', 'user', 'id', 'CASCADE');
	    $this->addForeignKey('fk-payout-toss_id', 'payout', 'toss_id', 'toss', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payout}}');
    }
}
