<?php
/**
 * @author Denysaw
 */
use yii\db\Migration;

/**
 * Handles the creation of table `{{%prize}}`.
 */
class m190419_185449_create_prize_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prize}}', [
            'id'   => $this->primaryKey(),
	        'type' => $this->string()->notNull()->unique(),
	        'is_available' => $this->boolean()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%prize}}');
    }
}
