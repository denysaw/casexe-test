<?php
/**
 * @author Denysaw
 */
namespace console\controllers;

use Yii;
use common\models\Toss;
use common\models\Prize;
use yii\console\Controller;
use common\jobs\MoneyTransferJob;

/**
 * Class SeedController
 * @package console\controllers
 */
class MoneyTransferController extends Controller
{

    public function actionIndex($batch = 100)
    {
        echo "Starting to trasfer money with batches of " . $batch . "Kč\n";

        $prize  = Prize::findOne(['type' => Prize::TYPE_MONEY]);
        $tosses = Toss::findAll(['prize_id' => $prize->id, 'status' => Toss::STATUS_PENDING]);
        $count  = 0;

        /** @var Toss $toss */
        foreach ($tosses as $toss) {
            $account = $toss->user->account;
            $total   = $toss->data;

            while ($total) {
                $amount = $total > $batch ? $batch : $total;
                $total -= $amount;

                Yii::$app->queue->push(new MoneyTransferJob(['account' => $account, 'amount' => $amount, 'toss_id' => $toss->id]));
                $count++;
            }
        }

        echo $count. ' money transfer' . ($count > 1 ? 's' : '') . ' scheduled';
    }
}