<?php
/**
 * @author Denysaw
 */
namespace console\controllers;

use Yii;
use common\models\Gift;
use common\models\Prize;
use common\models\Toss;
use common\models\User;
use common\models\Budget;
use yii\console\Controller;
use tebazil\yii2seeder\Seeder;

/**
 * Class SeedController
 * @package console\controllers
 */
class SeedController extends Controller
{

    public function actionIndex()
    {
        $seeder = new Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();
        $types = Yii::$app->params['prizeTypes'];
        $statuses = array_keys(Yii::$app->params['toss.statuses']);

        Yii::$app->db->createCommand('set foreign_key_checks=0')->execute();

        Prize::deleteAll();

        $seeder->table(Budget::tableName())->columns([
            'id',
            'name'   => 'main',
            'amount' => 1000000,
        ])->rowQuantity(1);

        $seeder->table(User::tableName())->columns([
            'id',
            'username'      => $faker->unique()->userName,
            'auth_key'      => $faker->md5,
            'password_hash' => $faker->md5,
            'email'         => $faker->unique()->email,
            'account'       => $faker->unique()->bankAccountNumber,
            'address'       => $faker->address,
        ])->rowQuantity(100);

        $seeder->table(Gift::tableName())->columns([
            'id',
            'name'     => $faker->unique()->firstName,
            'price'    => $faker->numberBetween(100, 5000),
            'quantity' => $faker->randomNumber(2),
        ])->rowQuantity(100);

        $seeder->table(Toss::tableName())->columns([
            'id',
            'user_id'  => $faker->numberBetween(1, 51),
            'prize_id' => $faker->numberBetween(1, 3),
            'data'     => $faker->numberBetween(1, 100),
            'status'   => $faker->randomElement($statuses),
        ])->rowQuantity(200);

        $seeder->refill();

        Yii::$app->db->createCommand('set foreign_key_checks=1')->execute();

        $user = new User();
        $user->username = 'test';
        $user->auth_key = 'test';
        $user->email = 'test@ya.ru';
        $user->account = '123456789';
        $user->address = 'I.P. Pavlova, Praha 2';
        $user->status = User::STATUS_ACTIVE;
        $user->setPassword('test');
        $user->save();

        foreach ($types as $type) {
            $prize = new Prize();
            $prize->type = $type;
            $prize->save();
        }
    }
}