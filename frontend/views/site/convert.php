<?php
/**
 * @author Denysaw
 */
use common\models\Prize;
use yii\helpers\Url;
use common\models\Toss;

/* @var $this yii\web\View */
$this->title = $state ? 'Успех' : 'Ошибка';

/** @var Toss $toss */
$type = $toss->prize->type;
?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?= $this->title;?></h1>
        <p class="lead">
            <?php if ($type == Prize::TYPE_MONEY):?>
                Ваши бонусы успешно сконвертированы в деньги
            <?php else:?>
                Ваши деньги успешно сконвертированы в бонусы
            <?php endif;?>
        </p>
        <p><a class="btn btn-sm btn-success" href="<?= Url::home();?>">Сыграть ещё раз</a></p>
    </div>
</div>