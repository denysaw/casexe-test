<?php
/**
 * @author Denysaw
 */
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'CASEXE Test App';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Жми кнопку!</h1>
        <p class="lead">И выигрывай призы!</p>
        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute(['won']);?>">Получить приз</a></p>
    </div>
</div>
