<?php
/**
 * @author Denysaw
 */
use yii\helpers\Url;
use common\models\Toss;
use common\models\Prize;

/** @var Toss $toss */
$prize   = $toss->prize;
$refused = $toss->status == Toss::STATUS_REFUSED;
$details = 'Что Вы отказались от приза.';

/* @var $this yii\web\View */
$this->title = $refused ? 'Очень жаль :(' : 'Поздравляем!';

if (!$refused) {
    $map = [
        Prize::TYPE_MONEY => 'Вскоре деньги будут переведены Вам на счёт.',
        Prize::TYPE_TOPUP => 'Вскоре баллы будут зачислены на Ваш баланс.',
        Prize::TYPE_GIFT  => 'Вскоре подарок будет отправлен Вам курьерской службой.',
    ];

    $details = $map[$prize->type];
}
?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?= $this->title;?></h1>
        <p class="lead"><?= $details;?></p>
        <p>Спасибо за участие!</p>
        <?php if (!$refused && in_array($prize->type, [Prize::TYPE_MONEY, Prize::TYPE_TOPUP])):?>
            <p>
                <a class="btn btn-sm btn-warning" href="<?= Url::toRoute(['convert', 'toss_id' => $toss->id]);?>">
                    Конвертировать приз
                </a>
            </p>
        <?php endif;?>
        <p><a class="btn btn-sm btn-success" href="<?= Url::home();?>">Сыграть ещё раз</a></p>
    </div>
</div>
